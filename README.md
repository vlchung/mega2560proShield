# Arduino Mega 2560 Pro Shield Outline

The Mega 2560 Pro ("Pro") is an "embedded" version of the standard
Arduino Mega 2560. The Pro is otherwise identical to the
Mega 2560, except it has been compressed to allow it to be
used in more permanent situations:


![Image](httprobotdyn.compubmedia0g-00005641mega-pro-ch340gatmega2560photophotoangle0g-00005641mega-pro-ch340gatmega2560no-pinheaders.jpg)

I have been unable to find a copy of a kicad based template that could be
used to create a shield that stacks onto the Pro, so using the mechanical
drawings I've found, I've created a very rough-and-ready PCB that can be
used as a template in kicad to create your own shield.

Note that this has not been testing on the real thing as yet. Another thing
to note:

![Image](DIM==0G-00005641==MEGA-PRO-CH340GATmega2560.jpg)

It is my belief that the mechnical drawing is not accurate. 38(.1)mm is the
distance between the first column of pins on the left to the last column
of pins on the right (for example, from D31 to D46). To obtain the actual
width of the board, one must add 2.54mm (1.27mm per side) to account for
the distance between the centre points of the holes at each end, making the
board 54mm x 40.64mm.

Some of the pins have also been labelled, useful for people like me
that like to dive straight into PCB editing without a schematic.

Some screenshots:

![Image](screenshotFull.png)

![Image](screenshotPins.png)

Feel free to submit merge requests. Happy to update the template if more
information is added.
